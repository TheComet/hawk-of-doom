----------------------------------------------------------------
-- Premake File - Builds project files
----------------------------------------------------------------

----------------------------------------------------------------
-- Global directories to include
----------------------------------------------------------------

local solutionIncludeDirs = {
	"include",
	"include/lib",
	"include/main",
	"$(SFML_HOME)/include"
}

----------------------------------------------------------------
-- Hawk Of Doom
----------------------------------------------------------------

-- Project name
solution "hawk-of-doom"

	-- Configurations
	configurations {
		"Debug",
		"Release"
	}

	-- Where to generate project files
	location "project"

	----------------------------------------------------------------
	-- Main application
	----------------------------------------------------------------

	project "hawk-of-doom"
		kind "ConsoleApp"
		language "C++"

		-- Files to include
		files {
			"src/main/**.cpp",
			"include/main/**.hpp"
		}

		-- header search directories
		includedirs (solutionIncludeDirs)

		-- global definitions
		defines {
			"SFML_DYNAMIC"
		}

		-- Debug configuration
		configuration "Debug"
			targetdir "bin/debug"
			defines {
				"DEBUG",
				"_DEBUG"
			}
			flags {
				"Symbols"
			}
			libdirs {
				"bin/lib",
				"$(SFML_HOME)/lib"
			}
			links {
				"hawk-of-doom-lib_d",
				"sfml-main-d",
				"sfml-audio-d",
				"sfml-graphics-d",
				"sfml-system-d",
				"sfml-window-d"
			}

		-- Release configuration
		configuration "Release"
			targetdir "bin/release"
			defines {
				"NDEBUG"
			}
			flags {
				"Optimize"
			}
			libdirs {
				"bin/lib"
			}
			links {
				"hawk-of-doom-lib"
			}

		----------------------------------------------------------------
		-- Main game library
		----------------------------------------------------------------

		project "hawk-of-doom-lib"
			kind "StaticLib"
			language "C++"
			files {
				"src/lib/**.cpp",
				"include/lib/**.hpp"
			}

			-- Header search directories
			includedirs (solutionIncludeDirs)

			-- Debug configuration
			configuration "Debug"
				targetdir "bin/lib"
				targetsuffix "_d"
				defines {
					"DEBUG",
					"_DEBUG"
				}
				flags {
					"Symbols"
				}
				libdirs {
				--	"$(SFML_HOME)/lib"
				}
				links {
				}

			-- release configuration
			configuration "Release"
				targetdir "bin/lib"
				defines {
					"NDEBUG"
				}
				flags {
					"Optimize"
				}
				libdirs {
				--	"$(SFML_HOME)/lib"
				}
				links {
				}

