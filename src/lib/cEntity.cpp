// -------------------------------------------------------------------
// Entity base class
// -------------------------------------------------------------------

// -------------------------------------------------------------------
// include files
#include <cEntity.hpp>

namespace HawkOfDoom {

// -------------------------------------------------------------------
// constructor
cEntity::cEntity( void ) :
	m_pDestroyMe( false )
{
}

// -------------------------------------------------------------------
// destructor
cEntity::~cEntity( void )
{
}

// -------------------------------------------------------------------
// returns true if the entity should be destroyed by the entity manager
bool cEntity::isDestroyed( void )
{
	return this->m_pDestroyMe;
}

// -------------------------------------------------------------------
// destroys an entity
void cEntity::destroy( void )
{
	this->m_pDestroyMe = true;
}

// -------------------------------------------------------------------
// update loop - to be overridden
void cEntity::update( void )
{
}

} // namespace HawkOfDoom