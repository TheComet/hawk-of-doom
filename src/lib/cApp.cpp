// -------------------------------------------------------------------
// Include files
#include <cApp.hpp>
#include <iostream>
#include <cException.hpp>

namespace HawkOfDoom {

// -------------------------------------------------------------------
// Constructor
cApp::cApp( int width, int height, int depth, bool isFullscreen, bool useVsync ) :
	m_pMainWindow( NULL ),
	m_pClock( NULL ),
	m_pEvent( NULL ),
	m_pTextManager( NULL ),
	m_pLoopCounter( 0 )
{

	// debug
	std::cout << "constructed cApp" << std::endl;

	// setup window
	if( isFullscreen )
	{
		this->m_pMainWindow = new sf::RenderWindow( sf::VideoMode( width, height ), "8 Bit Challenge", sf::Style::Fullscreen );
	}else{
		this->m_pMainWindow = new sf::RenderWindow( sf::VideoMode( width, height ), "8 Bit Challenge", sf::Style::Close | sf::Style::Titlebar );
	}
	this->m_pMainWindow->setVerticalSyncEnabled( useVsync );

	this->m_pClock = new sf::Clock();
	this->m_pEvent = new sf::Event();
	this->m_pTextManager = new cTextManager();
	this->m_pEntityManager = new cEntityManager();
}

// -------------------------------------------------------------------
// Destructor
cApp::~cApp( void )
{
	// debug
	std::cout << "destructed cApp" << std::endl;

	if( this->m_pMainWindow ) delete this->m_pMainWindow;
	if( this->m_pClock ) delete this->m_pClock;
	if( this->m_pEvent ) delete this->m_pEvent;
	if( this->m_pTextManager ) delete this->m_pTextManager;
	if( this->m_pEntityManager ) delete this->m_pEntityManager;
}

// -------------------------------------------------------------------
// loads all resources
void cApp::load( void )
{
	this->m_pTextManager->load();
}

// -------------------------------------------------------------------
// main entry point of the application
void cApp::go( void )
{

	std::string file = "media/images/scenery/test.png";
	cEntity* myEntity = this->m_pEntityManager->create( file );
	myEntity->setPosition( sf::Vector2f( 100, 100 ) );

	cText* myText = this->m_pTextManager->createText( "Sebastian is gaaayyy", 0 );

	// restart clock
	this->m_pClock->restart();

	// application runs as long as the window is open
	while( this->m_pMainWindow->isOpen() )
	{

		// calculate how many loops should have theoretically passed
		sf::Time totalTimePassed = this->m_pClock->getElapsedTime();
		unsigned long targetLoopCount = totalTimePassed.asMilliseconds()/17;

		// process game loop until target loop count has been reached
		while( this->m_pLoopCounter < targetLoopCount )
		{
			this->updateGame();
			m_pLoopCounter++;
		}

		// render game
		this->renderGame();

		// poll window events
		while( this->m_pMainWindow->pollEvent( *this->m_pEvent ) )
		{

			// close was requested
			if( this->m_pEvent->type == sf::Event::Closed )
				this->m_pMainWindow->close();
		}
	}
}

// -------------------------------------------------------------------
// updates game loop
void cApp::updateGame( void )
{
	this->m_pEntityManager->update();
	this->m_pTextManager->update();
}

// -------------------------------------------------------------------
// renders game
void cApp::renderGame( void )
{

	// clear window
	this->m_pMainWindow->clear( sf::Color::Black );

	// render things
	this->m_pEntityManager->render( this->m_pMainWindow );
	this->m_pTextManager->render( this->m_pMainWindow );

	// update
	this->m_pMainWindow->display();
}

} // namespace HawkOfDoom
