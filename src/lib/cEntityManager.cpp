// -------------------------------------------------------------------
// Manages all entity objects
// -------------------------------------------------------------------

// -------------------------------------------------------------------
// include files
#include <cEntityManager.hpp>

namespace HawkOfDoom {

// -------------------------------------------------------------------
// constructor
cEntityManager::cEntityManager( void )
{
	this->m_pEntityList = new std::vector<cEntity*>();
	this->m_pTextureManager = new cTextureManager();
}

// -------------------------------------------------------------------
// destructor
cEntityManager::~cEntityManager( void )
{
	delete this->m_pEntityList;
	delete this->m_pTextureManager;
}

// -------------------------------------------------------------------
// updates all entities
void cEntityManager::update( void )
{
	std::vector<cEntity*>::iterator it = this->m_pEntityList->begin();
	while( it != this->m_pEntityList->end() )
	{

		// request to be destroyed
		if( (*it)->isDestroyed() )
		{
			delete (*it);
			it = this->m_pEntityList->erase( it );

		// increment to next entity
		}else{
			it++;
		}
	}
}

// -------------------------------------------------------------------
// renders all entities
void cEntityManager::render( sf::RenderWindow* window )
{

	// rendering all vertices will render all entities
	window->draw( *this->m_pEntityVertexArray );
}

// -------------------------------------------------------------------
// create entity
cEntity* cEntityManager::create( const std::string& fileName )
{

	// load texture for entity
	cTexture* newTexture = this->m_pTextureManager->load( fileName );

	// create sprite with texture
	cEntity* newEntity = new cEntity();
	newEntity->setTexture( *newTexture->getTexture() );

	// add entity to list
	this->m_pEntityList->push_back( newEntity );

	// return entity
	return newEntity;
}

} // namespace HawkOfDoom