// -------------------------------------------------------------------
// Handles loading of textures and makes sure the same files are
// only loaded once
// -------------------------------------------------------------------

// -------------------------------------------------------------------
// include files
#include <cTextureManager.hpp>
#include <iostream>

namespace HawkOfDoom {

// -------------------------------------------------------------------
// constructor
cTextureManager::cTextureManager( void ) :
	m_pTextureList( NULL )
{
	// debug
	std::cout << "constructed cTextureManager" << std::endl;

	this->m_pTextureList = new std::vector<cTexture*>();
}

// -------------------------------------------------------------------
// destructor
cTextureManager::~cTextureManager( void )
{
	// debug
	std::cout << "destructed cTextManager" << std::endl;

	// clean up any textures loaded in memory
	this->cleanUp();

	delete this->m_pTextureList;
}

// -------------------------------------------------------------------
// loads a texture and returns a pointer to said texture
cTexture* cTextureManager::load( const std::string& fileName )
{

	// first check if texture has been loaded before
	for( std::vector<cTexture*>::iterator it = this->m_pTextureList->begin(); it != this->m_pTextureList->end(); it++ )
	{
		if( (*it)->getTextureName() == fileName ) return (*it);
	}

	// create new texture
	cTexture* newTexture = new cTexture();
	newTexture->load( fileName );
	this->m_pTextureList->push_back( newTexture );
	return newTexture;
}

// -------------------------------------------------------------------
// deletes all loaded textures
void cTextureManager::cleanUp( void )
{
	std::vector<cTexture*>::iterator it = this->m_pTextureList->begin();
	while( it != this->m_pTextureList->end() )
	{
		delete (*it);
		it = this->m_pTextureList->erase( it );
	}
}


} // namespace HawkOfDoom