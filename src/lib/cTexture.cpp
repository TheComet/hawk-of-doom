// -------------------------------------------------------------------
// Extends on the sf::Texture class
// -------------------------------------------------------------------

// -------------------------------------------------------------------
// include files
#include <cTexture.hpp>
#include <cException.hpp>
#include <iostream>

namespace HawkOfDoom {

// -------------------------------------------------------------------
// constructor
cTexture::cTexture( void ) :
	m_pFileName( "" )
{
	// debug
	std::cout << "constructed cTexture" << std::endl;

	// create texture object
	this->m_pTexture = new sf::Texture();
}

// -------------------------------------------------------------------
// destructor
cTexture::~cTexture( void )
{
	// debug
	std::cout << "destructed cTexture" << std::endl;

	// delete texture object
	delete this->m_pTexture;
}

// -------------------------------------------------------------------
// load texture
void cTexture::load( const std::string& fileName )
{
	if( !this->m_pTexture->loadFromFile( fileName ) )
	{
		throw cException();
	}
	this->m_pFileName = fileName;
}

// -------------------------------------------------------------------
// returns a pointer to the texture object
sf::Texture* cTexture::getTexture( void )
{
	return this->m_pTexture;
}

// -------------------------------------------------------------------
// returns the texture name
std::string& cTexture::getTextureName( void )
{
	return this->m_pFileName;
}

} // namespace HawkOfDoom