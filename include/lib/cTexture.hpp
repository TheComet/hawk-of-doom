// -------------------------------------------------------------------
// Composition with sf::Texture so more information about the
// files loaded can be stored
// -------------------------------------------------------------------

#ifndef _CTEXTURE_HPP_
#define _CTEXTURE_HPP_

// -------------------------------------------------------------------
// include files
#include <SFML/Graphics.hpp>
#include <string>

namespace HawkOfDoom {

class cTexture
{
public:

	// constructor, destructor
	cTexture( void );
	~cTexture( void );

	// returns the texture object
	sf::Texture* getTexture( void );

	// returns the name of the texture
	std::string& getTextureName( void );

	// loads a texture
	void load( const std::string& fileName );

private:

	// texture object
	sf::Texture* m_pTexture;

	// stores the path to the file loaded for later comparison
	std::string m_pFileName;
};

} // namespace HawkOfDoom

#endif // _CTEXTURE_HPP_