// -------------------------------------------------------------------
// Entity base class
// -------------------------------------------------------------------

#ifndef _CENTITY_HPP_
#define _CENTITY_HPP_

// -------------------------------------------------------------------
// include files
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

namespace HawkOfDoom {

class cSprite :
	public sf::Drawable,
	public sf::Transformable
{
public:

	// constructor, destructor
	cEntity( void );
	virtual ~cEntity( void );

	// update loop - to be overridden by inheriting classes
	virtual void update( void );

	// draws the sprite
	virtual void draw( sf::RenderTarget* target, sf::RenderStates states ) const;

protected:

	// adds a quad to the vertex array
	void addQuad( void );

private:

	// holds the list of vertices
	std::vector<sf::Vertex> m_VertexArray;

	// holds a pointer to the texture, stored in the texture manager
	sf::Texture* m_pTexture;
};

} // namespace HawkOfDoom

#endif // _CENTITY_HPP_