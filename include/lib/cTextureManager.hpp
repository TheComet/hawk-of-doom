// -------------------------------------------------------------------
// Handles loading of textures and makes sure the same files are
// only loaded once
// -------------------------------------------------------------------

#ifndef _CTEXTUREMANAGER_HPP_
#define _CTEXTUREMANAGER_HPP_

// -------------------------------------------------------------------
// include files
#include <cTexture.hpp>

namespace HawkOfDoom {

class cTextureManager
{
public:

	// constructor, destructor
	cTextureManager( void );
	~cTextureManager( void );

	// loads a texture into memory
	cTexture* load( const std::string& fileName );
	
private:

	// list of texture objects and names
	std::vector<cTexture*>* m_pTextureList;

	// cleans up all loaded textures
	void cleanUp( void );

};

} // namespace HawkOfDoom

#endif // _CTEXTMANAGER_HPP_