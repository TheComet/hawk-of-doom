// -------------------------------------------------------------------
// Manages all entity objects
// -------------------------------------------------------------------

#ifndef _CENTITYMANAGER_HPP_
#define _CENTITYMANAGER_HPP_

// -------------------------------------------------------------------
// include files
#include <cEntity.hpp>
#include <cTextureManager.hpp>
#include <iostream>

namespace HawkOfDoom {

class cEntityManager
{
public:

	// constructor, destructor
	cEntityManager( void );
	~cEntityManager( void );

	// calls the update loop of all entities
	void update( void );

	// renders all entities to the screen
	void render( sf::RenderWindow* window );

	// creates an entity
	cEntity* createEntity( const std::string& fileName );

	// destroys an entity
	void destroyEntity( cEntity* entity );

private:

	// list of entities
	std::vector<cEntity*>* m_pEntityList;

	// texture manager
	cTextureManager* m_pTextureManager;

};

} // namespace HawkOfDoom

#endif // _CENTITYMANAGER_HPP_